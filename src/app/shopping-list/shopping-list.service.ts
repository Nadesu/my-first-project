import { Ingredient } from '../shared/ingredinet.model';
import { Subject } from 'rxjs';

export class ShoppingListService{
    ingredientsChanged = new Subject<Ingredient[]>();
    startedEditing = new Subject<number>();

    private ingredients: Ingredient [] = [
        new Ingredient('Apple', 5),
        new Ingredient('Tomatoes', 10),
      ];

    addIngredient(ingredient: Ingredient ){
        this.ingredients.push(ingredient);
        this.ingredientsChanged.next(this.ingredients.slice());
    }

    getIngredients(){
        return this.ingredients.slice();
    }

    getIngredient(index: number){
        return this.ingredients[index];

    }

    addIngredients(ingredients: Ingredient[]){
        //for(let ingredient of ingredients){
        //    this.addIngredient(ingredient);
        //}

        //ES6 version spread operator 
        //The operator’s shape is three consecutive dots and is written as: ...
        this.ingredients.push(...ingredients);
        this.ingredientsChanged.next(this.ingredients.slice());
    }

    updateIngredient(index: number, newIngredient: Ingredient){
        this.ingredients[index] = newIngredient;
        this.ingredientsChanged.next(this.ingredients.slice());

    }

    deleteIngredient(index: number){
        this.ingredients.splice(index, 1);
        this.ingredientsChanged.next(this.ingredients.slice());

    }

}